# Instructions 
1. Download the [.zip file](https://bitbucket.org/wrossmck/timelapse-for-mac/downloads)
2. Unzip it.
3. Open terminal and run ```sh ~/Downloads/timelapse.sh```
4. If you need to change options, open the file up, and change the relevant options.
5. when you're done, [run Dan's software](http://www.dayofthenewdan.com/projects/time-lapse-assembler-1/) to convert it into a movie file


## Default Options:

Duration:	the # of pictures you will take	```1500 cycles or pictures```  
Interval:	the time between pictures	```5 seconds```  
Save location:	where the pictures will be	```"~/Pictures/timelapse/"```  

Final video duration ```1,500 pictures @ 25 fps = 60 seconds```.


## To Assemble into a Real Video...

1. My script - to generate the images periodically  
2. [This software by Dan](http://www.dayofthenewdan.com/projects/time-lapse-assembler-1/) to turn it into a movie / timelapse / screenlapse.


### Notes 
As it stands, the script works. It's not pretty, but it outputs a heap of images for as long as you want it to go on. 

I *may* consider making a cocoa GUI for this one day. For now, it's fully functional as a script.

I made a [timelapse of me making the timelapse script](http://youtu.be/lnL3XmPW7V8), which is available on youtube. Mostly so you can see it in action, but also to make sure it worked! It's only about 10 seconds long, but gives an idea of what can be done. I used a 5 second interval.


### ToDo
1. Pass command line params to the script, to specify interval, save-location, duration.

2. Incorporate [Dan's tlassemble](www.dayofthenewdan.com/projects/tlassemble/‎) into the code, so the movie will auto-generate after the timelimit has been reached.
