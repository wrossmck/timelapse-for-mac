#!/bin/bash
#VERSION 0.3
#Author = wrossmck
extention=".jpg"                                #can be changed to ".png" to output to .png
duration=1500                                   #can also be stopped by ctrl+c
interval=5                                      #seconds between new screencap
cd $HOME"/Pictures"
mkdir "timelapse"							#save to Pictures/screenlapse
cd "timelapse"


echo -en "\n\nTimeLapse for Mac\n"
echo -en "by wrossmck\n"
echo -en "\n\n"
echo -en "Saving $extention's to $PWD\n"


while [  $duration -gt 0 ]; do
    dateFormat="$(date +%Y_%m_%d)-$(date +%H_%M_%S)"
    screencapture -xtjpg "$dateFormat$extention" # -x = do not play sounds
	sleep $interval                             #outputs a screencap like  2013_02_29-13_11_01.png
	let duration=duration-1                     #update rate = 1 picture per $interval seconds.
	echo -en "\r$duration captures remaining." 		#let the user know that something is happening
done